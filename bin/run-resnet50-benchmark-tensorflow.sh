#!/usr/bin/env bash

docker run \
  -e TF_FORCE_GPU_ALLOW_GROWTH=true \
  --gpus all \
  -it \
  --rm \
  -v /home/${USER}/Documents/benchmarks:/benchmarks \
  tensorflow/tensorflow:2.0.1-gpu-py3-jupyter \
  python /benchmarks/scripts/tf_cnn_benchmarks/tf_cnn_benchmarks.py --num_gpus=1 --model resnet50 --batch_size 64
