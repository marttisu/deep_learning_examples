#!/usr/bin/env bash

tag="${1:-dl-sandbox}"

docker run \
  -e TF_FORCE_GPU_ALLOW_GROWTH=true \
  --gpus all \
  -it \
  -p 6006:6006 \
  --rm \
  -u $(id -u):$(id -g) \
  -v ${PWD}/src:/app/src \
  -v ${PWD}/data:/app/data \
  -w /app \
  $tag \
  tensorboard --bind_all --logdir=/app/data/logs
