#!/usr/bin/env bash

tag="${1:-dl-sandbox}"

docker run \
  -e TF_FORCE_GPU_ALLOW_GROWTH=true \
  --gpus all \
  -it \
  -p 8888:8888 \
  --rm \
  -u $(id -u):$(id -g) \
  -v ${PWD}/src:/app/src \
  -v ${PWD}/data:/app/data \
  -w /app \
  $tag \
  jupyter notebook --ip=0.0.0.0 --notebook-dir=/app --port=8888
