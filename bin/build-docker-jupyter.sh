#!/usr/bin/env bash

tag="${1:-dl-sandbox}"

docker build -f docker/Dockerfile -t $tag .
