"""Test Tensorflow."""
import datetime
import time
import sys

import tensorflow as tf

LOG_DIR = "/app/data/logs/"


def load_mnist_data():
    """Load MNIST data."""
    # https://www.tensorflow.org/tutorials/quickstart/beginner
    mnist = tf.keras.datasets.mnist
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0
    return x_train, x_test, y_train, y_test


def get_model():
    """Get model."""
    model = tf.keras.models.Sequential([
      tf.keras.layers.Flatten(input_shape=(28, 28)),
      tf.keras.layers.Dense(128, activation='relu'),
      tf.keras.layers.Dropout(0.2),
      tf.keras.layers.Dense(10)
    ])
    loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    model.compile(optimizer='adam',
                  loss=loss_fn,
                  metrics=['accuracy'])
    return model


def train_mnist(model, x_train, x_test, y_train, y_test, epochs=10):
    """Train model."""
    # https://www.tensorflow.org/tensorboard/get_started
    # TRAIN, EVALUATE
    log_dir = LOG_DIR + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(
        log_dir=log_dir, histogram_freq=1)
    model.fit(x=x_train,
              y=y_train,
              epochs=epochs,
              validation_data=(x_test, y_test),
              callbacks=[tensorboard_callback])


def main(args=[10]):
    """Run main function."""
    epochs = int(args[0])
    start_time = time.time()
    print('Starting MNIST MLP example with {:d} epochs'.format(
        epochs))
    print('- Loading data')
    x_train, x_test, y_train, y_test = load_mnist_data()
    model = get_model()
    print('- Training network')
    train_mnist(model, x_train, x_test, y_train, y_test, epochs=epochs)
    print('- Testing performance')
    model.evaluate(x_test, y_test, verbose=2)
    print('\nDone, took {:.3f} seconds'.format(
        time.time() - start_time))


if __name__ == '__main__':
    main(sys.argv[1:])
