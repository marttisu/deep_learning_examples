"""Try PyTorch with Iris dataset."""

import numpy as np
from sklearn.datasets import load_iris
from sklearn.metrics import accuracy_score, log_loss
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, StandardScaler
import sys
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

# module constants
TRAIN_SIZE = 0.8
SKLEARN_RANDOM_STATE = 0
MLP_LAYER_SIZES = [4, 100, 50, 3]
TORCH_OPTIM_LR = 0.01
TORCH_OPTIM_MOMENTUM = 0.5
# CUDA
USE_CUDA = torch.cuda.is_available()
DEVICE = torch.device("cuda:0" if USE_CUDA else "cpu")
DEVICE_NAME = torch.cuda.get_device_name(
    torch.cuda.current_device()) if USE_CUDA else "CPU"
if USE_CUDA:
    torch.cuda.init()
    # https://github.com/pytorch/pytorch/issues/21092
    torch.cuda.set_device(DEVICE)
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    Warning('GPU not available')


class MLP(nn.Module):
    """Multilayer perceptron class."""

    def __init__(self, sizes, activation_fn=F.relu):
        """Multilayer perceptron init."""
        super(MLP, self).__init__()
        # Hidden layers
        self.hidden_layers = nn.ModuleList()
        # Activation function
        self.activation_fn = activation_fn
        for i, n_nodes in enumerate(sizes[:-2]):
            self.hidden_layers.append(nn.Linear(n_nodes, sizes[i+1]))
        # Output layer
        self.output = nn.Linear(sizes[-2], sizes[-1])

    def forward(self, x):
        """Forward function."""
        for layer in self.hidden_layers:
            x = self.activation_fn(layer(x))
        output = self.output(x)
        return output


def predict_proba_label(network, X):
    """Predict probability and class."""
    X = torch.from_numpy(X).float()
    if USE_CUDA:
        X = X.cuda()
    proba = network(X).cpu()
    _, label = torch.max(proba, 1)
    return proba, label


def log_loss_accuracy(network, X, Y):
    """Get accuracy and log loss."""
    proba, label = predict_proba_label(network, X)
    Y_label = torch.argmax(torch.from_numpy(Y), 1)
    pred_log_loss = log_loss(Y, proba.data)
    pred_accuracy = accuracy_score(Y_label, label.data)
    return pred_log_loss, pred_accuracy


def load_iris_data():
    """Load Iris dataset."""
    # Load data
    iris = load_iris()
    X = iris['data']
    y = iris['target']
    # One hot encoding for target
    enc = OneHotEncoder()
    Y = enc.fit_transform(y[:, np.newaxis]).toarray()
    # Scale feature data to have mean 0 and variance 1
    scaler = StandardScaler()
    X_scaled = scaler.fit_transform(X)
    # Split the data set into training and testing
    X_train, X_test, Y_train, Y_test = train_test_split(
        X_scaled, Y,
        train_size=TRAIN_SIZE,
        random_state=SKLEARN_RANDOM_STATE)
    return X_train, X_test, Y_train, Y_test


def train_iris(X_train, Y_train, n_epoch=1000):
    """Train MLP for Iris classification."""
    network = MLP(sizes=MLP_LAYER_SIZES)
    if USE_CUDA:
        network.cuda()
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(
        network.parameters(),
        lr=TORCH_OPTIM_LR,
        momentum=TORCH_OPTIM_MOMENTUM)
    for epoch in range(n_epoch):
        inputs = torch.from_numpy(X_train).float()
        labels = torch.argmax(torch.from_numpy(Y_train), 1)
        if USE_CUDA:
            inputs = inputs.cuda()
            labels = labels.cuda()
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = network(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
    train_log_loss, train_accuracy = log_loss_accuracy(
        network, X_train, Y_train)
    return network, train_log_loss, train_accuracy


def test_iris(network, X_test, Y_test):
    """Test MLP for Iris classification."""
    test_log_loss, test_accuracy = log_loss_accuracy(
        network, X_test, Y_test)
    return test_log_loss, test_accuracy


def main(args=[1000]):
    """Run main function."""
    n_epoch = int(args[0])
    if USE_CUDA:
        print('Using device: {:s}'.format(DEVICE_NAME))
    else:
        print('GPU not available, using CPU')
    start_time = time.time()
    print('Starting Iris MLP example with {:d} epochs'.format(
        n_epoch))
    print('- Loading data')
    X_train, X_test, Y_train, Y_test = load_iris_data()
    print('- Training network')
    network, train_log_loss, train_accuracy = train_iris(
        X_train, Y_train, n_epoch=n_epoch)
    print('- Testing performance')
    test_log_loss, test_accuracy = test_iris(network, X_test, Y_test)
    print('''- Results:
      - Train log loss: {:.3f}
      - Train accuracy: {:.3f}
      - Test log loss: {:.3f}
      - Test accuracy: {:.3f}'''.format(
      train_log_loss, train_accuracy,
      test_log_loss, test_accuracy))
    print('Done, took {:.3f} seconds'.format(
        time.time() - start_time))


if __name__ == '__main__':
    main(sys.argv[1:])
