# deep_learning_examples

Purpose of this repo is to share scripts for easily setting up a local deep learning sandbox supporting GPU. The motivations are to 1) avoid conflicting requirements in different environments, 2) avoid installing CUDA Toolkit on host, and 3) make deployment easier. GPU compute capacity can be costly in the cloud so if you have a graphics card, you can use it locally for model training in hobby projects.

## Requirements

The scripts are created for [Ubuntu 20.04 LTS](https://releases.ubuntu.com/20.04/) with an NVIDIA card.

## Installation

The installation steps are
1. Install [docker](https://www.docker.com/)
1. Install [NVIDIA Container Toolkit](https://github.com/NVIDIA/nvidia-docker)
1. Try sandbox

Test your own code
1. Implement and test your own modules in container(s)
  * Rebuild container if you change requirements.txt
  * Optionally use a different tag as first command line argument to the build and run scripts
1. Use scripts in new projects

### Install docker

#### Install requirements and add key

```
sudo apt-get update \
  && sudo apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common
```

#### Add repo, install, and add permissions

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - \
  && sudo add-apt-repository -y \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable" \
  && sudo apt-get update \
  && sudo apt-get install -y docker-ce docker-ce-cli containerd.io \
  && sudo usermod -aG docker ${USER}
```

#### Log in again and test

```
su - ${USER}
docker run hello-world
```

### Install NVIDIA Container Toolkit

#### Add key, install, and restart docker

```
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
  && curl -s -L "https://nvidia.github.io/nvidia-docker/$(. /etc/os-release;echo $ID$VERSION_ID)/nvidia-docker.list" | sudo tee /etc/apt/sources.list.d/nvidia-docker.list \
  && sudo apt-get update \
  && sudo apt-get install -y nvidia-container-toolkit \
  && sudo systemctl restart docker
```

#### Run ResNet-50 benchmark with [TensorFlow](https://www.tensorflow.org/) in nvidia-docker

```
# download benchmark scripts locally to save results
git clone https://github.com/tensorflow/benchmarks.git /home/${USER}/Documents/benchmarks
# run benchmark in docker, mounting scripts
bin/run-resnet50-benchmark-tensorflow.sh
```

At the end, you should see output as below.
![alt text](img/resnet50.png "ResNet50 benchmark example output.")

### Try sandbox

#### Build and run container

Building can take a while because of the wide requirements, allowing to try [Keras](https://keras.io/), [PyTorch](https://pytorch.org/), and TensorFlow.

```
bin/build-docker-jupyter.sh
bin/run-docker-jupyter.sh
```

#### Run example notebooks

Then open the link with IP 127.0.0.1 to access [Jupyter](https://jupyter.org/) and run the notebook pytorch_example.

You should see output as below.
![alt text](img/jupyter.png "PyTorch example output.")

Run also the tensorflow_example. Then run Tensorboard in a new terminal: `bin/run-docker-tensorboard.sh`, and open [Tensorboard](http://127.0.0.1:6006/) to view training statistics such as below.

![alt text](img/tensorboard.png "Tensorboard example output.")


## Links

- [Installing TensorFlow 2 with GPU and Docker support on Ubuntu 20.04 LTS](https://illya13.github.io/RL/tutorial/2020/04/27/installing-tensorflow-in-docker-on-ubuntu-20.html)
- [How To Install and Use Docker on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
- [Use a GPU](https://www.tensorflow.org/guide/gpu)
- [CUDA semantics](https://pytorch.org/docs/stable/notes/cuda.html)
- [Deep Learning](http://www.deeplearningbook.org/)
